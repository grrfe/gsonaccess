# gsonaccess

Ever fetched a JSON payload and only needed a few values? Sick of having to cast everything to the correct types on every level of nesting? This library solves this problem by letting the user write "access rules" which are then used to parse the value needed. 

Knowledge of [GSON](https://github.com/google/gson) is very benefitial since it is used under the hood (as the name of this library suggests).

## Usable via
[![](https://jitpack.io/v/com.gitlab.grrfe/gsonaccess.svg)](https://jitpack.io/#com.gitlab.grrfe/gsonaccess)

## Examples

An example can be found [here](https://gitlab.com/grrfe/gsonaccess/-/blob/master/src/test/kotlin/fe/gsonaccess/example/Main.kt). 
