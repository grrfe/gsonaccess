package fe.gsonaccess.example

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.JsonPrimitive
import fe.gsonaccess.GsonAccess
import fe.gsonaccess.operation.ArrayAccessor
import fe.gsonaccess.operation.ArrayPath
import fe.gsonaccess.operation.ObjectKeyAccessor
import fe.gsonaccess.operation.ObjectPath

//payload from https://sochain.com/api/v2/get_price/DOGE/USD
val PAYLOAD = """{
          "status" : "success",
          "data" : {
            "network" : "DOGE",
            "prices" : [
              {
                "price" : "0.0742792",
                "price_base" : "USD",
                "exchange" : "binance",
                "time" : 1612781304
              }
            ]
          }
        }""".trimIndent()

//retrieves the price field
fun main() {
    //specify how to access the desired field
    val gsonAccess = GsonAccess(ObjectPath("data"), ArrayPath("prices"), ArrayAccessor(0), ObjectKeyAccessor("price"))

    //run all specified operations on the given payload
    val element = gsonAccess.applyOperations(JsonParser.parseString(PAYLOAD) as JsonObject)

    //all returns are JsonElement - price is primitive -> cast
    val primitive = element as JsonPrimitive
    println(primitive.asDouble)
}