package fe.gsonaccess

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import fe.gsonaccess.operation.Accessor
import fe.gsonaccess.operation.Path

class GsonAccess(private val operations: List<GsonAccessOperation>) {
    constructor(vararg operations: GsonAccessOperation) : this(operations.toList())

    fun applyOperations(root: JsonObject): JsonElement {
        var current: JsonElement = root
        operations.forEach { operation ->
            if (operation is Path<*>) {
                if (current is JsonObject) {
                    current = operation.followPath(current as JsonObject)
                }
            } else if (operation is Accessor) {
                current = operation.access(current)
            }
        }

        return current
    }
}

interface GsonAccessOperation

fun JsonObject.getIfHas(key: String): JsonElement? {
    return if (this.has(key)) this.get(key) else null
}