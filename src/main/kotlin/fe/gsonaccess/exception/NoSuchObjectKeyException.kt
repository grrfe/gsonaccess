package fe.gsonaccess.exception

import com.google.gson.JsonElement

class NoSuchObjectKeyException(key: String, element: JsonElement) : Exception("key=$key, element=$element")