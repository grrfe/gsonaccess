package fe.gsonaccess.exception

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import fe.gsonaccess.operation.Accessor

abstract class AccessorException(msg: String) : Exception(msg)

class ArrayAccessorIndexOutOfBoundsException(element: JsonArray, index: Int) : AccessorException("element=$element, index=$index, size=${element.size()}")

class InvalidTypeAccessorException(accessor: Accessor, element: JsonElement) :
    AccessorException("accessor=$accessor, element=$element")

