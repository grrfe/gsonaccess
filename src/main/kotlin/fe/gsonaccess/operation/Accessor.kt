package fe.gsonaccess.operation

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import fe.gsonaccess.GsonAccessOperation
import fe.gsonaccess.exception.ArrayAccessorIndexOutOfBoundsException
import fe.gsonaccess.exception.InvalidTypeAccessorException
import fe.gsonaccess.exception.NoSuchObjectKeyException
import fe.gsonaccess.getIfHas

fun interface Accessor : GsonAccessOperation {
    fun access(element: JsonElement): JsonElement
}

class ArrayAccessor(private val index: Int) : Accessor {
    override fun access(element: JsonElement): JsonElement {
        if (element is JsonArray) {
            if (index < element.size() && index >= 0) {
                return element.get(index)
            }

            throw ArrayAccessorIndexOutOfBoundsException(element, index)
        }

        throw InvalidTypeAccessorException(this, element)
    }
}

class ObjectKeyAccessor(private val key: String) : Accessor {
    override fun access(element: JsonElement): JsonPrimitive {
        if (element is JsonObject) {
            return element.getIfHas(key)?.let { it as JsonPrimitive } ?: throw NoSuchObjectKeyException(key, element)
        }

        throw InvalidTypeAccessorException(this, element)
    }
}