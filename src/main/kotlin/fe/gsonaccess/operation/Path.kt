package fe.gsonaccess.operation

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import fe.gsonaccess.GsonAccessOperation
import fe.gsonaccess.exception.NoSuchObjectKeyException
import fe.gsonaccess.getIfHas

abstract class Path<T : JsonElement>(private val key: String) : GsonAccessOperation {
    fun followPath(root: JsonObject): T {
        return root.getIfHas(key)?.let {
            this.cast(it)
        } ?: throw NoSuchObjectKeyException(key, root)
    }

    protected abstract fun cast(elem: JsonElement): T
}

class ObjectPath(key: String) : Path<JsonObject>(key) {
    override fun cast(elem: JsonElement) = elem as JsonObject
}

class ArrayPath(key: String) : Path<JsonArray>(key) {
    override fun cast(elem: JsonElement) = elem as JsonArray
}